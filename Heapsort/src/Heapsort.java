import java.util.Arrays;

public class Heapsort {
	public static void main(String[] args) {
		int[] list = { 1, 2, 3, 4 };

		System.out.println("Unsorted List:" + Arrays.toString(list));
		System.out.println("");
		heapsort(list, 0, list.length - 1);
		System.out.println("");
		System.out.println("Sorted List:" + Arrays.toString(list));
	}

	static void heapsort(int[] arr, int l, int r) {
		buildheap(arr, l, r);
		for (int i = r; i >= l + 1; i--) {
			int temp = arr[l];
			arr[l] = arr[i];
			arr[i] = temp;
			System.out.println("Heapsort Moving Array-Element:" + l + " > " + i);
			heapify(arr, l, l, i - 1);
		}
	}

	static void buildheap(int[] arr, int l, int r) {
		for (int i = r - l - 1; i >= 0; i--) {
			heapify(arr, l, l + i, r);
		}
	}

	static void heapify(int[] arr, int l, int q, int r) {
		int largest = l + 2 * (q - l) + 1;
		if (largest <= r) {
			if (largest < r && arr[largest + 1] > arr[largest]) {
				largest++;
			}
			if (arr[largest] > arr[q]) {
				int temp = arr[largest];
				arr[largest] = arr[q];
				arr[q] = temp;
				System.out.println("Heapify Moving Array-Element:" + largest + " > " + q);
				heapify(arr, l, largest, r);
			}
		}
	}

}
