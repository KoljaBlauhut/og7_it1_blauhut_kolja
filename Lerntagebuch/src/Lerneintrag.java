import java.util.Date;

public class Lerneintrag {
	private int dauer;
	private Date datum;
	private String fach;
	private String beschreibung;
	
	public Lerneintrag(int dauer, Date datum, String fach, String beschreibung) {
		this.dauer = dauer;
		this.datum = datum;
		this.fach = fach;
		this.beschreibung = beschreibung;
	}
	
	public Lerneintrag() {
		this.dauer = 0;
		this.datum = null;
		this.fach = null;
		this.beschreibung = null;
	}

	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public String getFach() {
		return fach;
	}
	public void setFach(String fach) {
		this.fach = fach;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	public int getDauer() {
		return dauer;
	}
	public void setDauer(int dauer) {
		this.dauer = dauer;
	}
	
	public String toString() {
		String temp = datum.toString() + " | ";
		temp += fach + " | ";
		temp += beschreibung + " | ";
		temp += dauer;
		return temp;
	}
}
