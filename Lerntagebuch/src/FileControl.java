import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileControl {
	private File file;
	private FileReader fr;
	private BufferedReader br;
	
	public FileControl(String path) {
		file = new File(path);
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> readFile() throws IOException {
		ArrayList<String> list = new ArrayList<String>();
		boolean epic = true;
		
		while (epic) {
			String str = br.readLine();
			if (str == null) {
				epic = false;
			} else {
				list.add(str);
			}
		}
		
		return list;
	}
}
