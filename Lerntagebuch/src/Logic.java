import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Logic {
	private FileControl fc;
	private List<Lerneintrag> lerneintraege;
	private String learnerName;
	
	public Logic(FileControl fc) throws IOException {
		this.fc = fc;
		reloadFromDisk();
	}
	
	public void reloadFromDisk() throws IOException {
		ArrayList<String> list = fc.readFile();
		learnerName = list.get(0);
		list.remove(0);
		
		Lerneintrag lerneintrag = new Lerneintrag();
		int eintraege = list.size()/4;
		
		for (int i = 0; i < eintraege; i++) {
			Date datum = new Date(list.get(3));
			lerneintrag.setFach(list.get(0));
			lerneintrag.setBeschreibung(list.get(1));
			lerneintrag.setDauer(Integer.parseInt(list.get(2)));
			lerneintrag.setDatum(datum);
			list.remove(0);
			list.remove(1);
			list.remove(2);
			list.remove(3);
			lerneintraege.add(lerneintrag);
		}
	}
	
	public List<Lerneintrag> getListEntries() {
		return lerneintraege;
	}
	
	public String getLearnerName() {
		return learnerName;
	}
}
