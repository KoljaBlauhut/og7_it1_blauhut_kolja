package omnom;

public class Haustier {
	private int hunger;
	private int muede;
	private int zufrieden;
	private int	gesund;
	private String name;
	
	Haustier(){
	hunger = 100;
	muede = 100;
	zufrieden = 100;
	gesund = 100;
	name = "omnom";
	}
	
	Haustier(String name){
	hunger = 100;
	muede = 100;
	zufrieden = 100;
	gesund = 100;
	this.name = name;
	}
	
	public void fuettern(int anzahl){
		int temp = anzahl + hunger;
		setHunger(temp);
	}
	
	public void schlafen(int anzahl){
		int temp = anzahl + muede;
		setMuede(temp);
	}
	
	public void spielen(int anzahl){
		int temp = anzahl + zufrieden;
		setZufrieden(temp);
	}
	
	public void heilen(){
		setGesund(100);
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger <= 0) {
			this.hunger = 0;
		} else if (hunger >= 100) {
			this.hunger = 100;
		} else
			this.hunger = hunger;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (muede <= 0) {
			this.muede = 0;
		} else if (muede >= 100) {
			this.muede = 100;
		} else
			this.muede = muede;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden <= 0) {
			this.zufrieden = 0;
		} else if (zufrieden >= 100) {
			this.zufrieden = 100;
		} else
			this.zufrieden = zufrieden;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		this.gesund = gesund;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
}
