package controller;

import java.util.LinkedList;
import java.util.List;

import model.Rechteck;

public class BunteRechteckeController {
	private List<Rechteck> rechtecke;
	private MySQLDatabase db = new MySQLDatabase();
	
	public BunteRechteckeController() {
		rechtecke = new LinkedList<Rechteck>();
	}
	
	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}
	
	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		db.rechteckEintragen(rechteck);
	}

	public void reset() {
		rechtecke.clear();
	}
	
	@Override
	public String toString() {
		String temp = "BunteRechteckeController [rechtecke=";
		int i = 0;
		for (Rechteck rechteck : rechtecke) {
			if (i != 0) temp += ",";
			temp += rechteck.toString();
			i++;
		}
		temp += "]";
		return temp;
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		reset();
		for (int i = 0; i < anzahl; i++) {
			add(Rechteck.generiereZufallsRechteck());	
		}	
	}
}
