package test;

import model.Rechteck;

import controller.BunteRechteckeController;


public class RechteckTest {

	public static void main(String[] args) {
		BunteRechteckeController controller =  new BunteRechteckeController();
		
		Rechteck rechteckTEST = new Rechteck();
		rechteckTEST = Rechteck.generiereZufallsRechteck();
		System.out.println(rechteckTEST.toString());

		
		Rechteck rechteck0 = new Rechteck(10,10,30,40);
		Rechteck rechteck1 = new Rechteck(25,25,100,20);
		Rechteck rechteck2 = new Rechteck(260,10,200,100);
		Rechteck rechteck3 = new Rechteck(5,500,300,25);
		Rechteck rechteck4 = new Rechteck(100,100,100,100);
		
		Rechteck rechteck5 = new Rechteck();
		rechteck5.setBreite(200);
		rechteck5.setHoehe(200);
		rechteck5.setX(200);
		rechteck5.setY(200);
		
		Rechteck rechteck6 = new Rechteck();
		rechteck6.setBreite(800);
		rechteck6.setHoehe(400);
		rechteck6.setX(20);
		rechteck6.setY(20);
		
		Rechteck rechteck7 = new Rechteck();
		rechteck7.setBreite(800);
		rechteck7.setHoehe(450);
		rechteck7.setX(20);
		rechteck7.setY(20);
		
		Rechteck rechteck8 = new Rechteck();
		rechteck8.setBreite(850);
		rechteck8.setHoehe(400);
		rechteck8.setX(20);
		rechteck8.setY(20);
		
		Rechteck rechteck9 = new Rechteck();
		rechteck9.setBreite(855);
		rechteck9.setHoehe(455);
		rechteck9.setX(25);
		rechteck9.setY(25);
		
		if(rechteck0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]")) System.out.println("ok");
		
		controller.add(rechteck0);
		controller.add(rechteck1);
		controller.add(rechteck2);
		controller.add(rechteck3);
		controller.add(rechteck4);
		controller.add(rechteck5);
		controller.add(rechteck6);
		controller.add(rechteck7);
		controller.add(rechteck8);
		controller.add(rechteck9);
		
		System.out.println(controller.toString());
	}
	
	public static boolean rechteckeTesten(Rechteck[] rechtecke) {
		Rechteck compare = new Rechteck(0,0,1200,1000);
		
		for (int i = 0; i < rechtecke.length; i++) {
			if (! compare.enthaelt(rechtecke[i]))
				return false;
		}
		return true;
	}
}
