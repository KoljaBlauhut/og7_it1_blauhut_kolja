package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import javax.swing.JPanel;
import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")
public class Zeichenflaeche extends JPanel {
	private BunteRechteckeController controller;
	
	public Zeichenflaeche(BunteRechteckeController controller) { 
		this.controller = controller;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		
		List<Rechteck> rechtecke = controller.getRechtecke();
		
		for (Rechteck rechteck : rechtecke) {
			g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
		}
	}

}
