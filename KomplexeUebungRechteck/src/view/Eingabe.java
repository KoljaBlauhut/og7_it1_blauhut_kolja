package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")
public class Eingabe extends JFrame {

	private JFrame frame;
	private JTextField txtBreite;
	private JTextField txtHoehe;
	private JTextField txtX;
	private JTextField txtY;
	private Rechteck rchtck;
	private BunteRechteckeController brc;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eingabe window = new Eingabe(new BunteRechteckeController());
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Eingabe(BunteRechteckeController brc) {
		this.brc = brc;
		initialize();
		frame.setVisible(true);
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		JButton btnReset = new JButton("Reset");
		panel.add(btnReset);
		
		JButton btnSubmit = new JButton("Submit");
		panel.add(btnSubmit);
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(9, 1, 0, 0));
		
		JLabel lblBreite = new JLabel("Breite");
		panel_1.add(lblBreite);
		
		txtBreite = new JTextField();
		panel_1.add(txtBreite);
		txtBreite.setColumns(10);
		
		JLabel lblHoehe = new JLabel("H\u00F6he");
		panel_1.add(lblHoehe);
		
		txtHoehe = new JTextField();
		panel_1.add(txtHoehe);
		txtHoehe.setColumns(10);
		
		JLabel lblX = new JLabel("X");
		panel_1.add(lblX);
		
		txtX = new JTextField();
		panel_1.add(txtX);
		txtX.setColumns(10);
		
		JLabel lblY = new JLabel("Y");
		panel_1.add(lblY);
		
		txtY = new JTextField();
		panel_1.add(txtY);
		txtY.setColumns(10);
		
		btnSubmit.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	rchtck = new Rechteck(parseInt(txtX.getText()), parseInt(txtY.getText()), parseInt(txtBreite.getText()), parseInt(txtHoehe.getText()));
		    	
		        txtX.setText("");
		        txtY.setText("");
		        txtBreite.setText("");
		        txtHoehe.setText("");
		        
		        brc.add(rchtck);
		        
		        System.out.println(brc.toString());
		    }

			private int parseInt(String text) {
				return Integer.parseInt(text);
			}
		});
	}
}
