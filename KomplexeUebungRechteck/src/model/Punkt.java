package model;

public class Punkt {
	private int x;
	private int y;
	
	public Punkt() {
		this.x = 0;
		this.y = 0;
	}
	
	public Punkt(int x, int y) {
		setX(x);
		setY(y);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public boolean equals(Punkt p) {
		return true;
	}
	
	public String toString() {
		return "";
	}
}
