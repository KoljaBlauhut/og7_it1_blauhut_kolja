package model;

import java.util.Random;

public class Rechteck {
	private int x;
	private int y;
	private int breite;
	private int hoehe;
	
	public Rechteck() {
		this.x = 0;
		this.y = 0;
		this.breite = 0;
		this.hoehe = 0;
		
	}
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		setX(x);
		setY(y);
		setBreite(breite);
		setHoehe(hoehe);
	}
	
	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		breite = Math.abs(breite);
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		hoehe = Math.abs(hoehe);
		this.hoehe = hoehe;
	}
	
	public boolean enthaelt(int x, int y) {
		if (x <= this.x && y <= this.y) {
			return true;
		}
		return false;
	}
	
	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(),p.getY());
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		if (enthaelt(rechteck.getX(), rechteck.getY()) && enthaelt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe())) {
			return true;
		}
		return false;
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Random rand = new Random();
		Rechteck temp = new Rechteck();
		temp.setX(rand.nextInt(1200));
		temp.setY(rand.nextInt(1000));
		temp.setBreite(rand.nextInt(1200 - temp.getX()));
		temp.setHoehe(rand.nextInt(1200 - temp.getY()));
		return temp;
	}
}
