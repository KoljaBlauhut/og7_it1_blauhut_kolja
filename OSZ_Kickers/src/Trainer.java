
public class Trainer extends Person {
	private double entschaedigung;
	private char lizenzklasse;
	
	public double getEntschaedigung() {
		return entschaedigung;
	}
	public void setEntschaedigung(double entschaedigung) {
		this.entschaedigung = entschaedigung;
	}
	public char getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
}
