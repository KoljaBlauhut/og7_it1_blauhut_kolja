
public class Manschaftsleiter extends Spieler {
	private String manschaftsname;
	private int rabatt;
	
	public String getManschaftsname() {
		return manschaftsname;
	}
	public void setManschaftsname(String manschaftsname) {
		this.manschaftsname = manschaftsname;
	}
	public int getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
}
