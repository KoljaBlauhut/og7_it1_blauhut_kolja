
public class KeyStore01 {
	private String[] arr;
	
	public KeyStore01() {
		arr = new String[100];
	}
	
	public KeyStore01(int length) {
		arr = new String[length];
	}
	
	public boolean add(String e) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == null) {
				arr[i] = e;
				return true;
			}
		}
		return false;
	}
	
	public void clear() {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = null;
		}
	}
	
	public String get(int index) {
		if (index < arr.length) {
			return arr[index];
		}
		return "";
	}
	
	public int indexOf(String str) {
		for (int i = 0; i < arr.length; i++) {
			if (str.equals(arr[i])) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean remove(int index) {
		for (int i = index; i < arr.length -1; i++) {
			arr[i] = arr[i+1];
		}
		return true;
	}
	
	public boolean remove(String str) {
		for (int i = 0; i < arr.length; i++) {
			if (str.equals(arr[i])) {
				return remove(i);
			}
		}
		return false;
	}
	
	public int size() {
		return arr.length;
	}
	
	public String toString() {
		String temp = "";
		for (int i = 0; i < arr.length; i++) {
			temp += arr[i] + ", ";
		}
		return temp;
	}
}
