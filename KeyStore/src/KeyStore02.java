import java.util.ArrayList;

public class KeyStore02 {
	private ArrayList<String> list;

	public KeyStore02() {
		list = new ArrayList<String>(100);
	}

	public KeyStore02(int length) {
		list = new ArrayList<String>(length);
	}

	public boolean add(String e) {
		return list.add(e);
	}

	public void clear() {
		list.clear();
	}

	public String get(int index) {
		return list.get(index);
	}

	public int indexOf(String str) {
		return list.lastIndexOf(str);
	}

	public boolean remove(int index) {
		list.remove(index);
		return true;
	}

	public boolean remove(String remove) {
		return list.remove(remove);
	}

	public int size() {
		return list.size();
	}

	public String toString() {
		String temp = "";
		for (int i = 0; i < list.size(); i++) {
			temp += list.get(i) + ", ";
		}
		return temp;
	}
}
