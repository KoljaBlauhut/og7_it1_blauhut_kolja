
public class Stopwatch {
	private long time = 0;
	
	public void startStopwatch() {
		time = System.currentTimeMillis();
	}
	
	public long stopStopwatch() {
		time = System.currentTimeMillis() - time;
		return time;
	}
	
	public void resetStopwatch() {
		time = 0;
	}
}
