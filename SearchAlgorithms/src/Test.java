
public class Test {
	public static void main(String[] args) {
		long time;
		int xPos;
		int x = 1999991;
		int[] list = SortedList.generateList(2000000, 1); 
		Stopwatch stopwatch = new Stopwatch();
		
		//Linear
		stopwatch.startStopwatch();
		xPos = Search.linearSearch(list, x);
		time = stopwatch.stopStopwatch();
		stopwatch.resetStopwatch();
		System.out.println("Linear Search");
		System.out.println("Time = " + time);
		System.out.println("Position of X = " + xPos);
		System.out.println("");
		
		
		//Fast
		stopwatch.startStopwatch();
		xPos = Search.fastSearch(0, list.length - 1, x, list);
		time = stopwatch.stopStopwatch();
		stopwatch.resetStopwatch();
		System.out.println("Fast Search");
		System.out.println("Time = " + time);
		System.out.println("Position of X = " + xPos);
	}
}
