
public class SortedList {
	public static int[] generateList(int size, int stepsize) {
		int[] array = new int[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = i * stepsize;
		}
		return array;
	}
}
