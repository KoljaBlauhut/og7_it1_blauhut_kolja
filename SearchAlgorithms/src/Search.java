
public class Search {
	public static int fastSearch(int von, int bis, int x, int[] list) {
		if (von <= bis) {
			int mB = (von + bis) / 2;
			int mI = von + ((bis - von) * (x - list[von]) / (list[bis] - list[von]));
			if (mB > mI) {
				int temp = mB;
				mB = mI;
				mI = temp;
			}
			if (x == list[mB]) {
				return mB;
			} else if (x == list[mI]) {
				return mI;
			} else if (x < list[mB]) {
				return fastSearch(von, mB-1, x, list);
			} else if (x < list[mI]) {
				return fastSearch(mB+1, mI-1, x, list);
			} else {
				return fastSearch(mI+1, bis, x, list);
			}
		}
		return -1;
	}
	
	public static int linearSearch(int[] list, int x) {
		for (int i = 0; i < list.length; i++) {
			if (list[i] == x)
			return i;
		}
		return -1;
	}
}
