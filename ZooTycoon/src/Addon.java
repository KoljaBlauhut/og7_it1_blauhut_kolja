public class Addon {
	private int idNummer;
	private int bestand;
	private int bestandMax;
	private double verkaufspreis;
	private String bezeichnung;
	
	public Addon() {
		idNummer = 420;
		bestand = 2;
		bestandMax = 99;
		verkaufspreis = 2.40;
		bezeichnung = "Bananen";
	}
	
	public int getIdNummer() {
		return idNummer;
	}
	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}
	public int getBestand() {
		return bestand;
	}
	public void setBestand(int bestand) {
		this.bestand = bestand;
	}
	public int getBestandMax() {
		return bestandMax;
	}
	public void setBestandMax(int bestandMax) {
		this.bestandMax = bestandMax;
	}
	public double getVerkaufspreis() {
		return verkaufspreis;
	}
	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public double gesamtwert() {
		return bestand * verkaufspreis;
	}
	
}
