import java.util.Arrays;

public class Bubblesort {

	public static void main(String[] args) {
		int[] list = {2,9,4,12,0,233,42,13,6};
		int temp;
		
		for (int i = 0; i < list.length; i++) {
			for (int j = 0; j < list.length; j++) {
				if (list[j] > list [i]) {
					temp = list[j];
					list[j] = list[i];
					list[i] = temp;
				}
			}
		}
		
		System.out.println(Arrays.toString(list));
	}

}
