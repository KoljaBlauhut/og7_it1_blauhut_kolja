
public class ArrayStuff {
 public static String convertArrayToString(int[] zahlen) {
	String returnValue = "";
	 for (int i = 0; i < zahlen.length; i++) {	
		if (i != zahlen.length - 1) {
			returnValue += String.valueOf(zahlen[i] + ", ");
		} else {
			returnValue += String.valueOf(zahlen[i]);
		}
	 }
	return returnValue;
 }
 
 public static int[] createIntArrayInitOne(int arrayLenght) {
	 int[] returnArray = new int[arrayLenght];
	 for (int i = 0; i < arrayLenght; i++) {
		returnArray[i] = 1;	
	 }
	 return returnArray;
 }
 
 public static int[] createIntArrayInitCountUp(int arrayLenght) {
	 int[] returnArray = new int[arrayLenght];
	 for (int i = 0; i < arrayLenght; i++) {
		 returnArray[i] = i;	
	 }
	 return returnArray;
 }
 
 public static int[] createIntArrayInitCountDown(int arrayLenght) {
	 int[] returnArray = new int[arrayLenght];
	 int temp = 0;
	 for (int i = arrayLenght - 1; i >= 0; i--) {
		 returnArray[i] = temp;
		 temp++;
	 }
	 return returnArray;
 }
 
 public static int[] createIntArrayInitUserNumbers() {
	 int[] returnArray = new int[5];
	 return returnArray;
 }
 
 public static void main(String[] args) {
	 String logIt = convertArrayToString(createIntArrayInitCountDown(9));
	 System.out.print(logIt);
 }
}
