
public class Stringz {
	public static int stringSearch(String[] arr, String x) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i].compareTo(x) == 0) {
				return i;
			}
		}
		return -1;
	}
	
	public static void stringSort(String[] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 1; j < arr.length; j++) {
				if (arr[j].compareTo(arr[j-1]) < 0) {
					String temp = arr[j];
					arr[j] = arr[j-1];
					arr[j-1] = temp;
				}
			}
		}
	}
	
	public static int stringSearchBinary(String[] arr, String x, int l, int r) {
		int m = (r-l)/2;
		
		if (m == l) {
			return -1;
		}
		
		if (arr[m].compareTo(x) == 0) {
			return m;
		}
		
		if (arr[m].compareTo(x) > 0) {
			return stringSearchBinary(arr, x, l, m);
		}
		
		if (arr[m].compareTo(x) < 0) {
			return stringSearchBinary(arr, x, m, r);
		}
		
		return -1;
	}
}
