
public class Zeitschrift extends Medium {
	private int nummer;
	private int jahrgang;
	private String issn;
	
	Zeitschrift(int nummer, int jahrgang, String issn) {
		this.nummer = nummer;
		this.jahrgang = jahrgang;
		this.issn = issn;
	}
	
	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}
	
	public int getJahrgang() {
		return jahrgang;
	}

	public void setJahrgang(int jahrgang) {
		this.jahrgang = jahrgang;
	}
	
	public String getISSN() {
		return issn;
	}

	public void setISSN(String issn) {
		this.issn = issn;
	}
}
