
public class Buch extends Medium {
	private String autor;
	private String isbn;
	
	public Buch(String autor, String titel, String isbn) {
		this.autor = autor;
		this.isbn = isbn;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getISBN() {
		return isbn;
	}

	public void setISBN(String isbn) {
		this.isbn = isbn;
	}

	
	@Override
	public String toString() {
		String temp = "";
		temp += "[autor=" + autor;
		temp += ", titel=" + titel;
		temp += ", isbn=" + isbn + "]";
		return  temp;
	}
}