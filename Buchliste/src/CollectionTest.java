import java.util.*;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.err.println("Autor angeben:");
				String autor = myScanner.next();
				System.err.println("Titel angeben:");
				String titel = myScanner.next();
				System.err.println("ISBN angeben:");
				String isbn = myScanner.next();
               Buch newBuch = new Buch(autor, titel, isbn);
               buchliste.add(newBuch);
				break;
			case '2':
				System.err.println("ISBN angeben:");
				eintrag = myScanner.next();
				if (findeBuch(buchliste, eintrag) != null) {
					Buch daBuch = findeBuch(buchliste, eintrag);
					System.err.println(daBuch.toString());
				} else {
					System.err.println("Buch wurde nicht gefunden.");
				}
				break;
			case '3':
				System.err.println("ISBN angeben:");
				eintrag = myScanner.next();
				if (findeBuch(buchliste, eintrag) != null) {
					Buch daBuch = findeBuch(buchliste, eintrag);
					loescheBuch(buchliste, daBuch);
				} else {
					System.err.println("Buch wurde nicht gefunden.");
				}
				break;
			case '4':
				System.err.println(ermitteleGroessteISBN(buchliste));
				break;		
			case '5':
				System.err.println(buchliste.toString());
				break;
			case '9':
				myScanner.close();
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
		} // switch

	} while (wahl != 9);
}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (Buch buch : buchliste) {
		    if (buch.getISBN().equals(isbn)) {
				return buch;
			}
		}
		
		return null;
	}

	public static boolean loescheBuch(List<Buch> buchliste, Buch b) {
		return buchliste.remove(b);
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		int temp = 0;
		for (int i = 0; i < buchliste.size(); i++) {
			Buch newBuch = buchliste.get(i);
			Buch oldBuch = buchliste.get(temp);
			if (newBuch.getISBN().compareTo(oldBuch.getISBN()) > 0) {
				temp = i;
			}
		}
		
		return buchliste.get(temp).getISBN();
	}

}