
public abstract class Film extends Medium{
	protected int laufzeit;
	
	public int getLaufzeit() {
		return laufzeit;
	}

	public void setLaufzeit(int laufzeit) {
		this.laufzeit = laufzeit;
	}
}
