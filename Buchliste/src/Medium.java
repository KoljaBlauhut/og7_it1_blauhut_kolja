
public abstract class Medium implements Comparable<Medium> {
	protected String titel;
	protected String ean;
	
	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getEAN() {
		return ean;
	}

	public void setEAN(String ean) {
		this.ean = ean;
	}
	
	@Override
	public boolean equals(Object o) {
		Medium temp = (Medium) o;
		if (this.ean.equals(temp.getEAN())) {
			return true;
		}
		return false;
	}

	@Override
	public int compareTo(Medium aMedium) {
		if (this.equals(aMedium)) {
			return 0;
		}
		return 1;
	}
}
